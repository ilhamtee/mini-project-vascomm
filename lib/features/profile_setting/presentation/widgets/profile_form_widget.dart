import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mini_project_vascomm/core/widgets/app_text_form_field.dart';

class ProfileFormWidget extends StatelessWidget {
  ProfileFormWidget({super.key});

  final TextEditingController _firstNameController =
      TextEditingController(text: 'Ilham Taufiqurrahman');
  final TextEditingController _lastNameController =
      TextEditingController(text: 'Salim');
  final TextEditingController _emailController =
      TextEditingController(text: 'ilhamsalim30@gmail.com');
  final TextEditingController _phoneNumberController =
      TextEditingController(text: '08782355123');
  final TextEditingController _ktpController =
      TextEditingController(text: '198319381110002');

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 10,
          ),
          child: AppTextFormField(
            controller: _firstNameController,
            hintText: 'Masukkan nama depan anda',
            label: 'Nama Depan',
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 10,
          ),
          child: AppTextFormField(
            controller: _lastNameController,
            hintText: 'Masukkan nama depan anda',
            label: 'Nama Belakang',
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 10,
          ),
          child: AppTextFormField(
            controller: _emailController,
            label: 'Email',
            hintText: 'Masukkan email anda',
            keyboardType: TextInputType.emailAddress,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 10,
          ),
          child: AppTextFormField(
            controller: _phoneNumberController,
            label: 'No. Telpon',
            hintText: 'Masukkan No. Telpon anda',
            keyboardType: TextInputType.number,
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 10,
          ),
          child: AppTextFormField(
            controller: _ktpController,
            label: 'No. KTP',
            hintText: 'Masukkan no KTP anda',
            keyboardType: TextInputType.number,
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          ),
        ),
      ],
    );
  }
}
