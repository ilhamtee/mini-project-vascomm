import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/constants/image_constant.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';

class HeaderProfileWidget extends StatelessWidget {
  const HeaderProfileWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: const BoxDecoration(
            color: AppColors.primary,
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                leading: const CircleAvatar(
                  radius: 25,
                  backgroundColor: Colors.white,
                  backgroundImage: AssetImage(ImageConstant.avatar),
                ),
                title: RichText(
                  text: TextSpan(
                    text: 'Ilham ',
                    style: Typographies.boldDefaultStyle
                        .copyWith(fontSize: 15, color: Colors.white),
                    children: const [
                      TextSpan(
                        text: 'Taufiqurrahman Salim',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
                subtitle: const Text(
                  'Flutter Developer',
                  style: TextStyle(
                    fontSize: 11,
                    color: AppColors.primary100,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                contentPadding: const EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 20,
                ),
              ),
              Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 12,
                ),
                decoration: const BoxDecoration(
                  color: AppColors.primary200,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                    topLeft: Radius.circular(20),
                  ),
                ),
                child: const Text(
                  'Lengkapi profile anda untuk memaksimalkan penggunaan aplikasi',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
        const Padding(
          padding: EdgeInsets.only(top: 20),
          child: Text(
            'Pilih data yang ingin ditampilkan',
            style: Typographies.boldDefaultStyle,
          ),
        ),
        const SizedBox(
          width: 300,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                leading: CircleAvatar(
                  radius: 20,
                  backgroundColor: AppColors.blue01,
                  child: Icon(
                    Icons.account_box,
                    color: AppColors.primary,
                    size: 20,
                  ),
                ),
                trailing: CircleAvatar(
                  radius: 20,
                  backgroundColor: AppColors.grey50,
                  child: Icon(
                    Icons.place,
                    color: AppColors.grey100,
                    size: 20,
                  ),
                ),
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                title: Text(
                  'Data Diri',
                  style: Typographies.boldDefaultStyle,
                ),
                subtitle: Text(
                  'Data diri anda sesuai KTP',
                  style: TextStyle(fontSize: 12),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
