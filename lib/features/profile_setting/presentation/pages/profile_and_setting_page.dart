import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';
import 'package:mini_project_vascomm/features/home/presentation/widgets/footer_home_widget.dart';
import 'package:mini_project_vascomm/features/profile_setting/presentation/widgets/header_profile_widget.dart';
import 'package:mini_project_vascomm/features/profile_setting/presentation/widgets/profile_form_widget.dart';
import 'package:toggle_switch/toggle_switch.dart';

class ProfileAndSettingPage extends StatefulWidget {
  const ProfileAndSettingPage({super.key});

  @override
  State<ProfileAndSettingPage> createState() => _ProfileAndSettingPageState();
}

class _ProfileAndSettingPageState extends State<ProfileAndSettingPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 20, top: 40),
          child: Material(
            elevation: 1.0,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20)),
            ),
            child: ToggleSwitch(
              initialLabelIndex: 0,
              cornerRadius: 20.0,
              activeBgColor: const [
                AppColors.blue01,
              ],
              customWidths: const [
                110,
                110,
              ],
              inactiveBgColor: Colors.white,
              totalSwitches: 2,
              labels: const ['Profile Saya', 'Pengaturan'],
              customTextStyles: const [
                TextStyle(
                  color: AppColors.primary,
                  fontWeight: FontWeight.bold,
                ),
                TextStyle(
                  color: AppColors.primary,
                  fontWeight: FontWeight.bold,
                ),
              ],
              radiusStyle: true,
            ),
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  margin:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Column(
                    children: [
                      const HeaderProfileWidget(),
                      ProfileFormWidget(),
                      const Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                        child: Row(
                          children: [
                            Icon(
                              Icons.warning_rounded,
                              color: AppColors.primary,
                              size: 15,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Expanded(
                              child: Text(
                                'Pastikan profile anda terisi dengan benar, data pribadi anda terjamin keamanannya',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 16,
                          right: 16,
                          bottom: 20,
                        ),
                        child: ElevatedButton(
                          onPressed: () async {},
                          style: ElevatedButton.styleFrom(
                            minimumSize: const Size(double.infinity, 45),
                          ),
                          child: Stack(
                            fit: StackFit.expand,
                            children: [
                              Align(
                                child: Text(
                                  'Simpan Profile',
                                  style:
                                      Typographies.regularDefaultStyle.copyWith(
                                    color: Colors.white,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                alignment: Alignment.center,
                              ),

                              const Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: EdgeInsets.only(right: 8.0),
                                  child: Icon(
                                    Icons.save,
                                    size: 15,
                                    color: Colors.white,
                                  ),
                                ),
                              ), // Icon di ujung kanan
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const FooterHomeWidget(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
