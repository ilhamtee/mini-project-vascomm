import 'package:equatable/equatable.dart';

class LoginRequestParams extends Equatable {
  const LoginRequestParams({
    required this.email,
    required this.password,
  });

  final String email;
  final String password;

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'password': password,
    };
  }

  @override
  List<Object?> get props => [
        email,
        password,
      ];
}
