import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mini_project_vascomm/features/authentication/domain/entities/login_entity.dart';

part 'login_model.codegen.g.dart';

@JsonSerializable()
class LoginModel extends LoginEntity {
  const LoginModel({String? token}) : super(token: token ?? '');

  factory LoginModel.fromJson(Map<String, dynamic> json) =>
      _$LoginModelFromJson(json);
}
