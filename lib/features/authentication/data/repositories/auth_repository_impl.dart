import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project_vascomm/core/utils/exception/exception.dart';
import 'package:mini_project_vascomm/features/authentication/data/data_sources/remote/auth_remote_datasource.dart';
import 'package:mini_project_vascomm/features/authentication/data/models/login_model.codegen.dart';
import 'package:mini_project_vascomm/features/authentication/data/models/login_request_params.dart';
import 'package:mini_project_vascomm/features/authentication/domain/repositories/auth_repository.dart';
import 'package:mini_project_vascomm/features/authentication/domain/use_cases/login_use_case.dart';

@LazySingleton(as: AuthRepository)
class AuthRepositoryImpl implements AuthRepository {
  const AuthRepositoryImpl(this._remoteDataSource);
  final AuthRemoteDataSource _remoteDataSource;

  @override
  Future<Either<Failures, LoginModel>> login(LoginUseCaseParam params) async {
    return await _remoteDataSource.login(
      LoginRequestParams(
        email: params.email,
        password: params.password,
      ),
    );
  }
}
