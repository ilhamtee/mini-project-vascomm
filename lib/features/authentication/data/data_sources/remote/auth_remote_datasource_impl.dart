import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mini_project_vascomm/core/constants/storage_constant.dart';
import 'package:mini_project_vascomm/core/constants/url_constant.dart';
import 'package:mini_project_vascomm/core/helpers/dio_helper.dart';
import 'package:mini_project_vascomm/core/utils/exception/exception.dart';
import 'package:mini_project_vascomm/core/utils/injector.dart';
import 'package:mini_project_vascomm/features/authentication/data/data_sources/remote/auth_remote_datasource.dart';
import 'package:mini_project_vascomm/features/authentication/data/models/login_model.codegen.dart';
import 'package:mini_project_vascomm/features/authentication/data/models/login_request_params.dart';

@LazySingleton(as: AuthRemoteDataSource)
class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {
  const AuthRemoteDataSourceImpl(this._dio);
  final Dio _dio;

  @override
  Future<Either<Failures, LoginModel>> login(LoginRequestParams params) async {
    try {
      final sharedPref = getIt<SharedPreferences>();

      final response = await _dio.post(
        URLConstant.login,
        data: params.toJson(),
      );
      if (response.statusCode == 200 || response.statusCode == 201) {
        /// Set User Session Info
        await sharedPref.setBool(StorageConstant.isLoggedInKey, true);
        return right(LoginModel.fromJson(response.data));
      }

      return left(
        ServerFailure(
          errorMessage: response.data['message'],
        ),
      );
    } on DioException catch (e) {
      final message = DioHelper.loginException(e);
      return Left(
        ServerFailure(
          errorMessage: message,
        ),
      );
    } catch (e) {
      return Left(
        UnexpectedFailure(
          errorMessage: e.toString(),
        ),
      );
    }
  }
}
