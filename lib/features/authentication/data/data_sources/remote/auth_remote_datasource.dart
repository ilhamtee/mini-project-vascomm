import 'package:dartz/dartz.dart';
import 'package:mini_project_vascomm/core/utils/exception/exception.dart';
import 'package:mini_project_vascomm/features/authentication/data/models/login_model.codegen.dart';
import 'package:mini_project_vascomm/features/authentication/data/models/login_request_params.dart';

abstract class AuthRemoteDataSource {
  Future<Either<Failures, LoginModel>> login(LoginRequestParams params);
}
