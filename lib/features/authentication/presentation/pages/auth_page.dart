import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_project_vascomm/core/constants/image_constant.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';
import 'package:mini_project_vascomm/core/utils/state/action_state.dart';
import 'package:mini_project_vascomm/core/widgets/toast_widget.dart';
import 'package:mini_project_vascomm/features/authentication/presentation/bloc/auth_bloc.dart';
import 'package:mini_project_vascomm/features/authentication/presentation/widgets/login_form_widget.dart';
import 'package:mini_project_vascomm/features/authentication/presentation/widgets/register_form_widget.dart';
import 'package:mini_project_vascomm/features/home/presentation/pages/home_page.dart';

class AuthenticationPage extends StatefulWidget {
  const AuthenticationPage({super.key});

  @override
  State<AuthenticationPage> createState() => _AuthenticationPageState();
}

class _AuthenticationPageState extends State<AuthenticationPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: BlocConsumer<AuthBloc, AuthState>(
          listener: (context, state) {
            if (state.loginStatus is ActionSuccess) {
              ToastWidget.show(
                message: 'Login Success',
                backgroundColor: AppColors.primary,
                textColor: Colors.white,
              );
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (_) => const HomePage()),
              );
            } else if (state.loginStatus is ActionError) {
              ToastWidget.show(
                message: (state.loginStatus as ActionError).error.toString(),
                backgroundColor: AppColors.primary,
                textColor: Colors.white,
              );
            }
          },
          builder: (context, state) {
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const _HeaderLoginWidget(),
                  if (state.authView == AuthenticationView.login)
                    LoginFormWidget()
                  else if (state.authView == AuthenticationView.register)
                    RegisterFormWidget(),
                  const Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.copyright_outlined,
                        color: AppColors.grey100,
                        size: 15,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        'SILK. all right reserved.',
                        style: TextStyle(
                          color: AppColors.grey100,
                          fontSize: 11,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

class _HeaderLoginWidget extends StatelessWidget {
  const _HeaderLoginWidget();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 100,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: RichText(
            text: TextSpan(
              text: 'Hai,',
              style: Typographies.regularDefaultStyle.copyWith(
                fontSize: 20,
                fontWeight: FontWeight.w500,
              ),
              children: [
                TextSpan(
                  text: ' Selamat Datang',
                  style: Typographies.boldDefaultStyle.copyWith(
                    fontSize: 20,
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 20, left: 16),
          child: Text(
            'Silahkan login untuk melanjutkan',
            style: Typographies.regularDefaultStyle
                .copyWith(color: AppColors.primary100),
          ),
        ),
        Container(
          width: double.infinity,
          alignment: Alignment.centerRight,
          child: Image.asset(
            ImageConstant.headerLogin,
            height: 200,
            width: 250,
            fit: BoxFit.fill,
          ),
        ),
      ],
    );
  }
}
