import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';
import 'package:mini_project_vascomm/core/widgets/app_text_form_field.dart';
import 'package:mini_project_vascomm/features/authentication/presentation/bloc/auth_bloc.dart';

class RegisterFormWidget extends StatelessWidget {
  RegisterFormWidget({super.key});

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordConfirmationController =
      TextEditingController();
  final TextEditingController _ktpController = TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // First Name & Last Name
        Padding(
          padding: const EdgeInsets.fromLTRB(
            16,
            10,
            16,
            15,
          ),
          child: Row(
            children: [
              Expanded(
                child: AppTextFormField(
                  controller: _firstNameController,
                  label: 'Nama Depan',
                  hintText: 'Masukkan nama depan',
                ),
              ),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                child: AppTextFormField(
                  controller: _lastNameController,
                  label: 'Nama Belakang',
                  hintText: 'Masukkan nama belakang',
                ),
              ),
            ],
          ),
        ),

        Padding(
          padding: const EdgeInsets.fromLTRB(
            16,
            20,
            16,
            15,
          ),
          child: AppTextFormField(
            controller: _ktpController,
            label: 'No. KTP',
            hintText: 'Masukkan No. KTP anda',
            keyboardType: TextInputType.number,
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          ),
        ),

        Padding(
          padding: const EdgeInsets.fromLTRB(
            16,
            20,
            16,
            15,
          ),
          child: AppTextFormField(
            controller: _emailController,
            label: 'Email',
            hintText: 'Masukkan email anda',
            keyboardType: TextInputType.emailAddress,
          ),
        ),

        Padding(
          padding: const EdgeInsets.fromLTRB(
            16,
            20,
            16,
            15,
          ),
          child: AppTextFormField(
            controller: _phoneController,
            label: 'No. Telpon',
            hintText: 'Masukkan No. Telpon anda',
            keyboardType: TextInputType.number,
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          ),
        ),

        Padding(
          padding: const EdgeInsets.fromLTRB(
            16,
            20,
            16,
            15,
          ),
          child: AppTextFormField(
            controller: _passwordController,
            label: 'Password',
            hintText: 'Masukkan password anda',
            isPassword: true,
          ),
        ),

        Padding(
          padding: const EdgeInsets.fromLTRB(
            16,
            20,
            16,
            15,
          ),
          child: AppTextFormField(
            controller: _passwordConfirmationController,
            label: 'Konfirmasi Password',
            hintText: 'Konfirmasi password anda',
            isPassword: true,
          ),
        ),

        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 20,
          ),
          child: ElevatedButton(
            onPressed: () {},
            style: ElevatedButton.styleFrom(
              minimumSize: const Size(double.infinity, 45),
            ),
            child: Stack(
              fit: StackFit.expand,
              children: [
                Align(
                  child: Text(
                    'Register',
                    style: Typographies.regularDefaultStyle.copyWith(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  alignment: Alignment.center,
                ),

                const Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(right: 8.0),
                    child: Icon(Icons.arrow_forward),
                  ),
                ), // Icon di ujung kanan
              ],
            ),
          ),
        ),

        Center(
          child: RichText(
            text: TextSpan(
              text: 'Sudah punya akun ? ',
              style: Typographies.regularDefaultStyle.copyWith(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: AppColors.grey100,
              ),
              children: [
                TextSpan(
                  text: 'Login Sekarang',
                  style: Typographies.boldDefaultStyle.copyWith(
                    fontSize: 14,
                  ),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      context.read<AuthBloc>().add(
                            const ChangeAuthViewEvent(
                              AuthenticationView.login,
                            ),
                          );
                    },
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
