import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';
import 'package:mini_project_vascomm/core/utils/state/action_state.dart';
import 'package:mini_project_vascomm/core/widgets/app_text_form_field.dart';
import 'package:mini_project_vascomm/features/authentication/presentation/bloc/auth_bloc.dart';

class LoginFormWidget extends StatelessWidget {
  LoginFormWidget({
    super.key,
  });

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(
            16,
            10,
            16,
            15,
          ),
          child: AppTextFormField(
            controller: _emailController,
            focusNode: _emailFocusNode,
            label: 'Email',
            hintText: 'Masukkan email anda',
            keyboardType: TextInputType.emailAddress,
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(
            16,
            20,
            16,
            15,
          ),
          child: AppTextFormField(
            controller: _passwordController,
            focusNode: _passwordFocusNode,
            label: 'Password',
            hintText: 'Masukkan passwrd anda',
            isPassword: true,
            isForgotPassword: true,
          ),
        ),
        BlocSelector<AuthBloc, AuthState, ActionState>(
          selector: (state) {
            return state.loginStatus;
          },
          builder: (context, loginStatus) {
            return Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 20,
              ),
              child: ElevatedButton(
                onPressed: loginStatus is ActionLoading
                    ? null
                    : () async {
                        _emailFocusNode.unfocus();
                        _passwordFocusNode.unfocus();
                        context.read<AuthBloc>().add(
                              LoginEvent(
                                email: _emailController.text,
                                password: _passwordController.text,
                              ),
                            );
                      },
                style: ElevatedButton.styleFrom(
                  minimumSize: const Size(double.infinity, 50),
                ),
                child: loginStatus is ActionLoading
                    ? const SizedBox.square(
                        dimension: 22.0,
                        child: Center(
                          child: CircularProgressIndicator.adaptive(
                            valueColor: AlwaysStoppedAnimation<Color>(
                              AppColors.primary,
                            ),
                          ),
                        ),
                      )
                    : Stack(
                        fit: StackFit.expand,
                        children: [
                          Align(
                            child: Text(
                              'Login',
                              style: Typographies.regularDefaultStyle.copyWith(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            alignment: Alignment.center,
                          ),

                          const Align(
                            alignment: Alignment.centerRight,
                            child: Padding(
                              padding: EdgeInsets.only(right: 8.0),
                              child: Icon(
                                Icons.arrow_forward,
                                color: Colors.white,
                              ),
                            ),
                          ), //
                        ],
                      ),
              ),
            );
          },
        ),
        Center(
          child: RichText(
            text: TextSpan(
              text: 'Belum punya akun ? ',
              style: Typographies.regularDefaultStyle.copyWith(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: AppColors.grey100,
              ),
              children: [
                TextSpan(
                  text: 'Daftar Sekarang',
                  style: Typographies.boldDefaultStyle.copyWith(
                    fontSize: 14,
                  ),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      /// Go to Register Page
                      context.read<AuthBloc>().add(
                            const ChangeAuthViewEvent(
                              AuthenticationView.register,
                            ),
                          );
                    },
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
