part of 'auth_bloc.dart';

enum AuthenticationView {
  login,
  register;
}

@freezed
class AuthState with _$AuthState {
  const factory AuthState({
    @Default(AuthenticationView.login) AuthenticationView authView,
    @Default(ActionState.initial()) ActionState loginStatus,
    @Default('') String token,
  }) = _AuthState;
}
