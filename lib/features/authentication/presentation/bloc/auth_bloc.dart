import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project_vascomm/core/utils/exception/exception.dart';
import 'package:mini_project_vascomm/core/utils/state/action_state.dart';
import 'package:mini_project_vascomm/features/authentication/domain/entities/login_entity.dart';
import 'package:mini_project_vascomm/features/authentication/domain/use_cases/login_use_case.dart';

part 'auth_event.dart';
part 'auth_state.dart';
part 'auth_bloc.freezed.dart';

@injectable
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc(this._loginUseCase) : super(const AuthState()) {
    on<LoginEvent>(_onLogin);
    on<ChangeAuthViewEvent>(_onChangeAuthView);
  }

  final LoginUseCase _loginUseCase;

  Future<void> _onLogin(LoginEvent event, emit) async {
    emit(state.copyWith(loginStatus: const ActionLoading()));

    final loginResult = await _loginUseCase(
      LoginUseCaseParam(
        email: event.email,
        password: event.password,
      ),
    );

    loginResult.fold(
      (Failures failures) => emit(
        state.copyWith(loginStatus: ActionError(failures.errorMessage)),
      ),
      (LoginEntity loginEntity) => emit(
        state.copyWith(
          loginStatus: const ActionSuccess(),
          token: loginEntity.token,
        ),
      ),
    );
  }

  void _onChangeAuthView(ChangeAuthViewEvent event, emit) {
    emit(
      state.copyWith(
        authView: event.authenticationView,
        loginStatus: const ActionInitial(),
      ),
    );
  }
}
