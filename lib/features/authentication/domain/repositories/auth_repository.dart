import 'package:dartz/dartz.dart';
import 'package:mini_project_vascomm/core/utils/exception/exception.dart';
import 'package:mini_project_vascomm/features/authentication/domain/entities/login_entity.dart';
import 'package:mini_project_vascomm/features/authentication/domain/use_cases/login_use_case.dart';

abstract class AuthRepository {
  Future<Either<Failures, LoginEntity>> login(LoginUseCaseParam params);
}
