import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project_vascomm/core/utils/exception/exception.dart';
import 'package:mini_project_vascomm/core/utils/usecase/use_case.dart';
import 'package:mini_project_vascomm/features/authentication/domain/entities/login_entity.dart';
import 'package:mini_project_vascomm/features/authentication/domain/repositories/auth_repository.dart';

@injectable
class LoginUseCase implements UseCase<LoginEntity, LoginUseCaseParam> {
  const LoginUseCase(this._repository);
  final AuthRepository _repository;

  @override
  Future<Either<Failures, LoginEntity>> call(
    LoginUseCaseParam params,
  ) async {
    return await _repository.login(params);
  }
}

class LoginUseCaseParam extends Equatable {
  const LoginUseCaseParam({
    required this.email,
    required this.password,
  });

  final String email;
  final String password;

  @override
  List<Object?> get props => [
        email,
        password,
      ];
}
