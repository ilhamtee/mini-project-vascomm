import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project_vascomm/core/constants/image_constant.dart';
import 'package:mini_project_vascomm/core/utils/state/action_state.dart';
import 'package:mini_project_vascomm/features/home/domain/entities/product_entity.dart';
import 'package:mini_project_vascomm/features/home/domain/entities/product_type_entity.dart';

part 'home_state.dart';
part 'home_cubit.freezed.dart';

@lazySingleton
class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(const HomeState());

  void initializeProducts() {
    final List<ProductTypeEntity> initialProductTypeList = <ProductTypeEntity>[
      const ProductTypeEntity(
        productTypeName: 'All Product',
        isSelected: true,
      ),
      const ProductTypeEntity(
        productTypeName: 'Layanan Kesehatan',
        isSelected: false,
      ),
      const ProductTypeEntity(
        productTypeName: 'Alat Kesehatan',
        isSelected: false,
      ),
      const ProductTypeEntity(productTypeName: 'Vaksinasi', isSelected: false),
    ];

    final List<ProductEntity> initialProductList = List.generate(
      5,
      (index) => const ProductEntity(
        productName: 'Suntik Steril',
        price: 130000,
        rating: 5.0,
        image: ImageConstant.product,
        isReadyStock: true,
      ),
    );

    emit(
      state.copyWith(
        productTypeList: initialProductTypeList,
        productList: initialProductList,
      ),
    );
  }

  void onSelectedProductType(ProductTypeEntity selectedType) {
    final List<ProductTypeEntity> updatedProductTypeList =
        state.productTypeList.map((e) {
      if (e.productTypeName == selectedType.productTypeName) {
        return ProductTypeEntity(
          productTypeName: e.productTypeName,
          isSelected: true,
        );
      }

      return ProductTypeEntity(
        productTypeName: e.productTypeName,
        isSelected: false,
      );
    }).toList();

    emit(state.copyWith(productTypeList: updatedProductTypeList));
  }

  Future<void> loadMoreServicePlace() async {
    if (state.loadServicePlaceStatus != const ActionLoading()) {
      emit(state.copyWith(loadServicePlaceStatus: const ActionLoading()));

      // Load more simulation
      await Future.delayed(const Duration(seconds: 2));

      emit(state.copyWith(loadServicePlaceStatus: const ActionSuccess()));
    }
  }

  void changeHomeMenuView(HomeMenu menu) {
    emit(state.copyWith(selectedHomeMenu: menu));
  }
}
