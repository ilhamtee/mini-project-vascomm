// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'home_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$HomeState {
  List<ProductEntity> get productList => throw _privateConstructorUsedError;
  List<ProductTypeEntity> get productTypeList =>
      throw _privateConstructorUsedError;
  ActionState get loadServicePlaceStatus => throw _privateConstructorUsedError;
  HomeMenu get selectedHomeMenu => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $HomeStateCopyWith<HomeState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeStateCopyWith<$Res> {
  factory $HomeStateCopyWith(HomeState value, $Res Function(HomeState) then) =
      _$HomeStateCopyWithImpl<$Res, HomeState>;
  @useResult
  $Res call(
      {List<ProductEntity> productList,
      List<ProductTypeEntity> productTypeList,
      ActionState loadServicePlaceStatus,
      HomeMenu selectedHomeMenu});
}

/// @nodoc
class _$HomeStateCopyWithImpl<$Res, $Val extends HomeState>
    implements $HomeStateCopyWith<$Res> {
  _$HomeStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productList = null,
    Object? productTypeList = null,
    Object? loadServicePlaceStatus = null,
    Object? selectedHomeMenu = null,
  }) {
    return _then(_value.copyWith(
      productList: null == productList
          ? _value.productList
          : productList // ignore: cast_nullable_to_non_nullable
              as List<ProductEntity>,
      productTypeList: null == productTypeList
          ? _value.productTypeList
          : productTypeList // ignore: cast_nullable_to_non_nullable
              as List<ProductTypeEntity>,
      loadServicePlaceStatus: null == loadServicePlaceStatus
          ? _value.loadServicePlaceStatus
          : loadServicePlaceStatus // ignore: cast_nullable_to_non_nullable
              as ActionState,
      selectedHomeMenu: null == selectedHomeMenu
          ? _value.selectedHomeMenu
          : selectedHomeMenu // ignore: cast_nullable_to_non_nullable
              as HomeMenu,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$HomeStateImplCopyWith<$Res>
    implements $HomeStateCopyWith<$Res> {
  factory _$$HomeStateImplCopyWith(
          _$HomeStateImpl value, $Res Function(_$HomeStateImpl) then) =
      __$$HomeStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<ProductEntity> productList,
      List<ProductTypeEntity> productTypeList,
      ActionState loadServicePlaceStatus,
      HomeMenu selectedHomeMenu});
}

/// @nodoc
class __$$HomeStateImplCopyWithImpl<$Res>
    extends _$HomeStateCopyWithImpl<$Res, _$HomeStateImpl>
    implements _$$HomeStateImplCopyWith<$Res> {
  __$$HomeStateImplCopyWithImpl(
      _$HomeStateImpl _value, $Res Function(_$HomeStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? productList = null,
    Object? productTypeList = null,
    Object? loadServicePlaceStatus = null,
    Object? selectedHomeMenu = null,
  }) {
    return _then(_$HomeStateImpl(
      productList: null == productList
          ? _value._productList
          : productList // ignore: cast_nullable_to_non_nullable
              as List<ProductEntity>,
      productTypeList: null == productTypeList
          ? _value._productTypeList
          : productTypeList // ignore: cast_nullable_to_non_nullable
              as List<ProductTypeEntity>,
      loadServicePlaceStatus: null == loadServicePlaceStatus
          ? _value.loadServicePlaceStatus
          : loadServicePlaceStatus // ignore: cast_nullable_to_non_nullable
              as ActionState,
      selectedHomeMenu: null == selectedHomeMenu
          ? _value.selectedHomeMenu
          : selectedHomeMenu // ignore: cast_nullable_to_non_nullable
              as HomeMenu,
    ));
  }
}

/// @nodoc

class _$HomeStateImpl implements _HomeState {
  const _$HomeStateImpl(
      {final List<ProductEntity> productList = const <ProductEntity>[],
      final List<ProductTypeEntity> productTypeList =
          const <ProductTypeEntity>[],
      this.loadServicePlaceStatus = const ActionState.initial(),
      this.selectedHomeMenu = HomeMenu.dashboard})
      : _productList = productList,
        _productTypeList = productTypeList;

  final List<ProductEntity> _productList;
  @override
  @JsonKey()
  List<ProductEntity> get productList {
    if (_productList is EqualUnmodifiableListView) return _productList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_productList);
  }

  final List<ProductTypeEntity> _productTypeList;
  @override
  @JsonKey()
  List<ProductTypeEntity> get productTypeList {
    if (_productTypeList is EqualUnmodifiableListView) return _productTypeList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_productTypeList);
  }

  @override
  @JsonKey()
  final ActionState loadServicePlaceStatus;
  @override
  @JsonKey()
  final HomeMenu selectedHomeMenu;

  @override
  String toString() {
    return 'HomeState(productList: $productList, productTypeList: $productTypeList, loadServicePlaceStatus: $loadServicePlaceStatus, selectedHomeMenu: $selectedHomeMenu)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HomeStateImpl &&
            const DeepCollectionEquality()
                .equals(other._productList, _productList) &&
            const DeepCollectionEquality()
                .equals(other._productTypeList, _productTypeList) &&
            (identical(other.loadServicePlaceStatus, loadServicePlaceStatus) ||
                other.loadServicePlaceStatus == loadServicePlaceStatus) &&
            (identical(other.selectedHomeMenu, selectedHomeMenu) ||
                other.selectedHomeMenu == selectedHomeMenu));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_productList),
      const DeepCollectionEquality().hash(_productTypeList),
      loadServicePlaceStatus,
      selectedHomeMenu);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$HomeStateImplCopyWith<_$HomeStateImpl> get copyWith =>
      __$$HomeStateImplCopyWithImpl<_$HomeStateImpl>(this, _$identity);
}

abstract class _HomeState implements HomeState {
  const factory _HomeState(
      {final List<ProductEntity> productList,
      final List<ProductTypeEntity> productTypeList,
      final ActionState loadServicePlaceStatus,
      final HomeMenu selectedHomeMenu}) = _$HomeStateImpl;

  @override
  List<ProductEntity> get productList;
  @override
  List<ProductTypeEntity> get productTypeList;
  @override
  ActionState get loadServicePlaceStatus;
  @override
  HomeMenu get selectedHomeMenu;
  @override
  @JsonKey(ignore: true)
  _$$HomeStateImplCopyWith<_$HomeStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
