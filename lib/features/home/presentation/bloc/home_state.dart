part of 'home_cubit.dart';

enum HomeMenu { dashboard, profileAndSetting }

@freezed
class HomeState with _$HomeState {
  const factory HomeState({
    @Default(<ProductEntity>[]) List<ProductEntity> productList,
    @Default(<ProductTypeEntity>[]) List<ProductTypeEntity> productTypeList,
    @Default(ActionState.initial()) ActionState loadServicePlaceStatus,
    @Default(HomeMenu.dashboard) HomeMenu selectedHomeMenu,
  }) = _HomeState;
}
