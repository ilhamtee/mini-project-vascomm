import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/widgets/app_text_form_field.dart';

class SearchAndFilterWidget extends StatelessWidget {
  const SearchAndFilterWidget({super.key, required this.searchController});

  final TextEditingController searchController;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Transform.translate(
          offset: const Offset(0.0, 5.0),
          child: Material(
            elevation: 3.0,
            shape: const CircleBorder(),
            child: DecoratedBox(
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Image.asset(
                  'assets/icons/filter.png',
                  width: 20,
                ),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 20,
        ),
        Expanded(
          child: AppTextFormField(
            controller: searchController,
            hintText: 'Search',
            elevation: 3.0,
            borderRadius: BorderRadius.circular(30.0),
            contentPadding: const EdgeInsets.symmetric(
              vertical: 18.0,
              horizontal: 20.0,
            ),
            suffixIcon: IconButton(
              onPressed: null,
              padding: const EdgeInsets.only(right: 10),
              icon: Image.asset(
                'assets/icons/search.png',
                width: 20,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
