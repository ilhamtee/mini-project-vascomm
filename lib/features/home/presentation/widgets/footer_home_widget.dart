import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';

class FooterHomeWidget extends StatelessWidget {
  const FooterHomeWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 107,
      color: AppColors.primary,
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Ingin mendapat update \ndari kami ?',
            style: Typographies.boldDefaultStyle
                .copyWith(color: Colors.white, fontSize: 18),
          ),
          Row(
            children: [
              Text(
                'Dapatkan \nnotifikasi',
                style: Typographies.regularDefaultStyle
                    .copyWith(color: Colors.white),
              ),
              const SizedBox(
                width: 5,
              ),
              const Icon(
                Icons.arrow_forward,
                color: Colors.white,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
