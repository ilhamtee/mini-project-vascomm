import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/constants/image_constant.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';

class TrackExaminationSectionWidget extends StatelessWidget {
  const TrackExaminationSectionWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 15,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Transform.translate(
                offset: const Offset(0.0, -50.0),
                child: Image.asset(
                  ImageConstant.track,
                  width: 110,
                  height: 120,
                  fit: BoxFit.contain,
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Track Pemeriksaan',
                    style:
                        Typographies.boldDefaultStyle.copyWith(fontSize: 16.0),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Kamu dapat mengecek progress \npemeriksaanmu disini',
                    style: Typographies.regularDefaultStyle.copyWith(
                      color: AppColors.primary100,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextButton(
                    onPressed: () {},
                    style: TextButton.styleFrom(
                      foregroundColor: AppColors.primary50,
                    ),
                    child: const Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          'Track',
                          style: Typographies.boldDefaultStyle,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Icon(
                          Icons.arrow_forward,
                          color: AppColors.primary,
                          size: 20,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      shadowColor: AppColors.primary100,
      surfaceTintColor: Colors.white,
      color: Colors.white,
      elevation: 1.0,
    );
  }
}
