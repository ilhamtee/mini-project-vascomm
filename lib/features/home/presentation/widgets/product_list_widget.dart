import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/helpers/number_formatter_helper.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';
import 'package:mini_project_vascomm/features/home/domain/entities/product_entity.dart';

class ProductListWidget extends StatelessWidget {
  const ProductListWidget({super.key, required this.productList});

  final List<ProductEntity> productList;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      padding: const EdgeInsets.only(left: 16, right: 16, bottom: 30),
      child: Row(
        children: productList
            .map(
              (product) => Padding(
                padding: const EdgeInsets.only(right: 10.0),
                child: Card(
                  elevation: 2.0,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  color: Colors.white,
                  surfaceTintColor: Colors.white,
                  child: SizedBox(
                    width: 180,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 10,
                        vertical: 15,
                      ),
                      child: Stack(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Center(
                                child: Image.asset(
                                  product.image,
                                  width: 140,
                                  height: 100,
                                  fit: BoxFit.contain,
                                ),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              Text(
                                product.productName,
                                style: Typographies.boldDefaultStyle,
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    NumberFormatterHelper.formatRupiah(
                                      product.price,
                                    ),
                                    style: const TextStyle(
                                      color: AppColors.orange01,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12,
                                    ),
                                  ),
                                  if (product.isReadyStock)
                                    const DecoratedBox(
                                      decoration: BoxDecoration(
                                        color: AppColors.green01,
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(
                                            5.0,
                                          ),
                                        ),
                                      ),
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                          horizontal: 5.0,
                                          vertical: 2.0,
                                        ),
                                        child: Text(
                                          'Ready Stock',
                                          style: TextStyle(
                                            fontSize: 10,
                                            color: AppColors.green02,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            ],
                          ),
                          Positioned(
                            top: 0,
                            right: 0,
                            child: Row(
                              children: [
                                const Icon(
                                  Icons.star,
                                  color: Colors.yellow,
                                ),
                                Text(
                                  product.rating.toString(),
                                  style: const TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
