import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/constants/image_constant.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';

class LatestInfoSectionWidget extends StatelessWidget {
  const LatestInfoSectionWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: const LinearGradient(
            colors: [
              Colors.white,
              Colors.white,
              AppColors.primary50,
            ],
          ),
        ),
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 15,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(
                    text: 'Solusi,',
                    style: Typographies.regularDefaultStyle.copyWith(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                    children: [
                      TextSpan(
                        text: ' Kesehatan Anda',
                        style: Typographies.boldDefaultStyle.copyWith(
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  'Update informasi seputar kesehatan \nsemua bisa disini !',
                  style: Typographies.regularDefaultStyle.copyWith(
                    color: AppColors.primary100,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    elevation: 0.0,
                  ),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 10,
                    ),
                    child: Text(
                      'Selengkapnya',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Transform.translate(
              offset: const Offset(20.0, -50.0),
              child: Image.asset(
                ImageConstant.calendarTimeline,
                width: 120,
                height: 120,
                fit: BoxFit.cover,
              ),
            ),
          ],
        ),
      ),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      shadowColor: AppColors.primary100,
      elevation: 5.0,
    );
  }
}
