import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_project_vascomm/core/constants/image_constant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';
import 'package:mini_project_vascomm/core/utils/injector.dart';
import 'package:mini_project_vascomm/features/authentication/presentation/pages/auth_page.dart';
import 'package:mini_project_vascomm/features/home/presentation/bloc/home_cubit.dart';

class DrawerHomeWidget extends StatelessWidget {
  const DrawerHomeWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 55,
          color: AppColors.primary.withOpacity(0.75),
          alignment: Alignment.topCenter,
          child: Container(
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            padding: const EdgeInsets.all(2),
            margin: const EdgeInsets.only(top: 25),
            child: GestureDetector(
              onTap: () => Navigator.pop(context),
              child: const Icon(
                Icons.close,
                color: AppColors.primary,
                size: 15,
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(
            color: Colors.white,
            padding: const EdgeInsets.only(left: 20, top: 100),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 250,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListTile(
                        leading: const CircleAvatar(
                          radius: 35,
                          backgroundImage: AssetImage(ImageConstant.avatar),
                        ),
                        title: RichText(
                          text: TextSpan(
                            text: 'Ilham ',
                            style: Typographies.boldDefaultStyle
                                .copyWith(fontSize: 15),
                            children: const [
                              TextSpan(
                                text: 'Taufiqurrahman Salim',
                                style: TextStyle(
                                  color: AppColors.primary,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        subtitle: const Text(
                          'Flutter Developer',
                          style: TextStyle(
                            fontSize: 11,
                            color: AppColors.primary100,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        height: 40,
                        child: ListTile(
                          title: const Text(
                            'Dashboard',
                            style: TextStyle(
                              color: AppColors.grey100,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          trailing: const Icon(
                            Icons.chevron_right_outlined,
                            color: AppColors.grey100,
                          ),
                          contentPadding:
                              const EdgeInsets.symmetric(horizontal: 20),
                          onTap: () {
                            context
                                .read<HomeCubit>()
                                .changeHomeMenuView(HomeMenu.dashboard);
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      SizedBox(
                        height: 40,
                        child: ListTile(
                          title: const Text(
                            'Profile Saya',
                            style: TextStyle(
                              color: AppColors.grey100,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          trailing: const Icon(
                            Icons.chevron_right_outlined,
                            color: AppColors.grey100,
                          ),
                          contentPadding:
                              const EdgeInsets.symmetric(horizontal: 20),
                          onTap: () {
                            context
                                .read<HomeCubit>()
                                .changeHomeMenuView(HomeMenu.profileAndSetting);
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      ListTile(
                        title: const Text(
                          'Pengaturan',
                          style: TextStyle(
                            color: AppColors.grey100,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        trailing: const Icon(
                          Icons.chevron_right_outlined,
                          color: AppColors.grey100,
                        ),
                        contentPadding:
                            const EdgeInsets.symmetric(horizontal: 20),
                        onTap: () {
                          context
                              .read<HomeCubit>()
                              .changeHomeMenuView(HomeMenu.profileAndSetting);
                          Navigator.pop(context);
                        },
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Center(
                        child: SizedBox(
                          width: 180,
                          child: ElevatedButton(
                            onPressed: () async {
                              /// Clear User Session Info
                              final sharedPref = getIt<SharedPreferences>();
                              await sharedPref.clear();

                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => const AuthenticationPage(),
                                ),
                              );
                            },
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.red,
                            ),
                            child: const Text(
                              'Logout',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 50,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Ikuti kami di ',
                      style:
                          Typographies.boldDefaultStyle.copyWith(fontSize: 12),
                    ),
                    const SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      'assets/icons/facebook.png',
                      width: 12,
                    ),
                    const SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      'assets/icons/instagram.png',
                      width: 13,
                    ),
                    const SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      'assets/icons/twitter.png',
                      width: 12,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
