import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/constants/image_constant.dart';
import 'package:mini_project_vascomm/core/helpers/number_formatter_helper.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';

class ServicePlaceItemWidget extends StatelessWidget {
  const ServicePlaceItemWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2.0,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(16)),
      ),
      color: Colors.white,
      surfaceTintColor: Colors.white,
      child: Row(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 12,
                horizontal: 16,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'PCR Swab Test (Drive Thru) \nHasil 1 Hari Kerja',
                    style: Typographies.boldDefaultStyle.copyWith(fontSize: 13),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Text(
                    NumberFormatterHelper.formatRupiah(1400000),
                    style: const TextStyle(
                      color: AppColors.orange01,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  const Row(
                    children: [
                      Icon(
                        Icons.location_city,
                        size: 18,
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        'Lenmarc Surabaya',
                        style: TextStyle(
                          color: AppColors.grey400,
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  const Row(
                    children: [
                      Icon(
                        Icons.place,
                        size: 18,
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        'Dukuh Pakis, Surabaya',
                        style: TextStyle(
                          color: AppColors.grey100,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(16)),
            child: Image.asset(
              ImageConstant.examinationPlace,
              fit: BoxFit.cover,
              width: 140,
              height: 185,
            ),
          ),
        ],
      ),
    );
  }
}
