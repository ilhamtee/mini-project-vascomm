import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/features/home/domain/entities/product_type_entity.dart';

class ProductTypeWrapperWidget extends StatelessWidget {
  const ProductTypeWrapperWidget({
    super.key,
    required this.productList,
    required this.onSelected,
  });

  final List<ProductTypeEntity> productList;
  final ValueChanged<ProductTypeEntity> onSelected;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      padding: const EdgeInsets.only(left: 16, right: 16, bottom: 20),
      child: Row(
        children: [
          Wrap(
            children: productList
                .map(
                  (product) => Theme(
                    data: Theme.of(context).copyWith(
                      splashColor: Colors.transparent,
                    ),
                    child: ChoiceChip(
                      label: Text(product.productTypeName),
                      selected: product.isSelected,
                      backgroundColor: Colors.white,
                      elevation: 2.0,
                      selectedColor: AppColors.primary,
                      checkmarkColor: Colors.white,
                      labelStyle: TextStyle(
                        color: product.isSelected
                            ? Colors.white
                            : AppColors.primary,
                        fontWeight: FontWeight.bold,
                      ),
                      padding: const EdgeInsets.symmetric(
                        horizontal: 15,
                        vertical: 10,
                      ),
                      onSelected: (_) {
                        onSelected(product);
                      },
                    ),
                  ),
                )
                .toList(),
            spacing: 8,
          ),
        ],
      ),
    );
  }
}
