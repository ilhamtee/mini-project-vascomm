import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/constants/image_constant.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';

class VaccineSectionWidget extends StatelessWidget {
  const VaccineSectionWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 15,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Layanan Khusus',
                    style:
                        Typographies.boldDefaultStyle.copyWith(fontSize: 16.0),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Tes Covid 19, Cegah Corona \nSedini Mungkin',
                    style: Typographies.regularDefaultStyle.copyWith(
                      color: AppColors.primary100,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextButton(
                    onPressed: () {},
                    style: TextButton.styleFrom(
                      foregroundColor: AppColors.primary50,
                    ),
                    child: const Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          'Daftar Test',
                          style: Typographies.boldDefaultStyle,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Icon(
                          Icons.arrow_forward,
                          color: AppColors.primary,
                          size: 20,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(
                width: 20,
              ),
              Transform.translate(
                offset: const Offset(0.0, -50.0),
                child: Image.asset(
                  ImageConstant.vaccine,
                  width: 120,
                  height: 120,
                  fit: BoxFit.cover,
                ),
              ),
            ],
          ),
        ),
      ),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      shadowColor: AppColors.primary100,
      elevation: 1.0,
      surfaceTintColor: Colors.white,
      color: Colors.white,
    );
  }
}
