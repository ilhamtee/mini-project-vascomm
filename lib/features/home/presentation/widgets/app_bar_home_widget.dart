import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';

class AppBarHomeWidget extends StatelessWidget implements PreferredSizeWidget {
  const AppBarHomeWidget({super.key, required this.isDrawerOpen});
  final bool isDrawerOpen;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(
        color: isDrawerOpen ? Colors.white : AppColors.primary,
      ),
      elevation: 0.0,
      scrolledUnderElevation: 0.0,
      actions: [
        Padding(
          padding: const EdgeInsets.only(right: 20),
          child: Image.asset(
            'assets/icons/cart.png',
            width: 20,
            height: 20,
            fit: BoxFit.contain,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Image.asset(
            'assets/icons/notification.png',
            width: 20,
            height: 20,
            fit: BoxFit.contain,
          ),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
