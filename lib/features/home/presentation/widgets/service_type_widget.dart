import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';
import 'package:toggle_switch/toggle_switch.dart';

class ServiceTypeWidget extends StatelessWidget {
  const ServiceTypeWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(left: 16, bottom: 20),
          child: Text(
            'Pilih Tipe Layanan Kesehatan Anda',
            style: Typographies.boldDefaultStyle,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16, bottom: 20),
          child: Material(
            elevation: 1.0,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20)),
            ),
            child: ToggleSwitch(
              initialLabelIndex: 0,
              minWidth: 90.0,
              cornerRadius: 20.0,
              activeBgColor: const [
                AppColors.blue01,
              ],
              customWidths: const [
                80,
                150,
              ],
              inactiveBgColor: Colors.white,
              totalSwitches: 2,
              labels: const ['Satuan', 'Paket Pemeriksaan'],
              customTextStyles: const [
                TextStyle(
                  color: AppColors.primary,
                  fontWeight: FontWeight.bold,
                ),
                TextStyle(
                  color: AppColors.primary,
                  fontWeight: FontWeight.bold,
                ),
              ],
              radiusStyle: true,
            ),
          ),
        ),
      ],
    );
  }
}
