import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/features/home/presentation/bloc/home_cubit.dart';
import 'package:mini_project_vascomm/features/home/presentation/pages/dashboard_page.dart';
import 'package:mini_project_vascomm/features/home/presentation/widgets/app_bar_home_widget.dart';
import 'package:mini_project_vascomm/features/home/presentation/widgets/drawer_home_widget.dart';
import 'package:mini_project_vascomm/features/profile_setting/presentation/pages/profile_and_setting_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _isDrawerOpen = false;

  @override
  Widget build(BuildContext context) {
    final HomeMenu selectedMenu =
        context.watch<HomeCubit>().state.selectedHomeMenu;

    return SafeArea(
      child: Scaffold(
        onDrawerChanged: (isOpen) {
          setState(() {
            _isDrawerOpen = isOpen;
          });
        },
        backgroundColor: selectedMenu == HomeMenu.dashboard
            ? Colors.white
            : AppColors.primary10,
        appBar: AppBarHomeWidget(isDrawerOpen: _isDrawerOpen),
        body: selectedMenu == HomeMenu.dashboard
            ? DashboardPage()
            : const ProfileAndSettingPage(),
        drawer: const DrawerHomeWidget(),
      ),
    );
  }
}
