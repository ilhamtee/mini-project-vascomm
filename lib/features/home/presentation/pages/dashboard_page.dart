import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/utils/state/action_state.dart';
import 'package:mini_project_vascomm/features/home/domain/entities/product_type_entity.dart';
import 'package:mini_project_vascomm/features/home/presentation/bloc/home_cubit.dart';
import 'package:mini_project_vascomm/features/home/presentation/widgets/footer_home_widget.dart';
import 'package:mini_project_vascomm/features/home/presentation/widgets/latest_info_section_widget.dart';
import 'package:mini_project_vascomm/features/home/presentation/widgets/product_list_widget.dart';
import 'package:mini_project_vascomm/features/home/presentation/widgets/product_type_wrapper_widget.dart';
import 'package:mini_project_vascomm/features/home/presentation/widgets/search_and_filter_widget.dart';
import 'package:mini_project_vascomm/features/home/presentation/widgets/service_place_item_widget.dart';
import 'package:mini_project_vascomm/features/home/presentation/widgets/service_type_widget.dart';
import 'package:mini_project_vascomm/features/home/presentation/widgets/track_examination_section_widget.dart';
import 'package:mini_project_vascomm/features/home/presentation/widgets/vaccine_section_widget.dart';

class DashboardPage extends StatelessWidget {
  DashboardPage({super.key});

  final TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      bloc: context.read<HomeCubit>(),
      builder: (context, state) {
        return LazyLoadScrollView(
          onEndOfPage: () {
            context.read<HomeCubit>().loadMoreServicePlace();
          },
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 50,
                      ),

                      // Latest information section
                      const LatestInfoSectionWidget(),

                      const SizedBox(
                        height: 50,
                      ),

                      // Vaccine section
                      const VaccineSectionWidget(),

                      const SizedBox(
                        height: 50,
                      ),

                      // Track examination section
                      const TrackExaminationSectionWidget(),

                      const SizedBox(
                        height: 30,
                      ),

                      // Search and Filter
                      SearchAndFilterWidget(
                        searchController: _searchController,
                      ),

                      const SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                ),
                ProductTypeWrapperWidget(
                  productList: state.productTypeList,
                  onSelected: (ProductTypeEntity type) {
                    context.read<HomeCubit>().onSelectedProductType(type);
                  },
                ),
                ProductListWidget(productList: state.productList),
                const SizedBox(
                  height: 12,
                ),
                const ServiceTypeWidget(),
                ListView.builder(
                  itemCount: 3,
                  physics: const NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16.0,
                    vertical: 15,
                  ),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return const Padding(
                      padding: EdgeInsets.only(bottom: 15),
                      child: ServicePlaceItemWidget(),
                    );
                  },
                ),

                // Load more loading indicator
                BlocSelector<HomeCubit, HomeState, ActionState>(
                  selector: (state) {
                    return state.loadServicePlaceStatus;
                  },
                  builder: (context, loadServicePlaceStatus) {
                    if (loadServicePlaceStatus is ActionLoading) {
                      return const Padding(
                        padding: EdgeInsets.only(bottom: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox.square(
                              dimension: 20,
                              child: CircularProgressIndicator.adaptive(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                  AppColors.grey100,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Tampilkan Lebih Banyak',
                              style: TextStyle(
                                color: AppColors.grey100,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      );
                    }

                    return const SizedBox.shrink();
                  },
                ),

                const FooterHomeWidget(),
              ],
            ),
          ),
        );
      },
    );
  }
}
