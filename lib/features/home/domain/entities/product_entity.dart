import 'package:equatable/equatable.dart';

class ProductEntity extends Equatable {
  const ProductEntity({
    required this.productName,
    required this.price,
    required this.rating,
    required this.image,
    required this.isReadyStock,
  });

  final String productName;
  final double price;
  final double rating;
  final String image;
  final bool isReadyStock;

  @override
  List<Object?> get props => [productName, price, rating, image, isReadyStock];
}
