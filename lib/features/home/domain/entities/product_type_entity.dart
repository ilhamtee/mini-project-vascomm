import 'package:equatable/equatable.dart';

class ProductTypeEntity extends Equatable {
  const ProductTypeEntity({
    required this.productTypeName,
    required this.isSelected,
  });

  final String productTypeName;
  final bool isSelected;

  @override
  List<Object?> get props => [productTypeName, isSelected];
}
