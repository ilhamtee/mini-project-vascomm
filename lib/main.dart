import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mini_project_vascomm/core/constants/storage_constant.dart';
import 'package:mini_project_vascomm/core/constants/url_constant.dart';
import 'package:mini_project_vascomm/core/helpers/dio_helper.dart';
import 'package:mini_project_vascomm/core/styles/app_theme.dart';
import 'package:mini_project_vascomm/features/authentication/presentation/bloc/auth_bloc.dart';
import 'package:mini_project_vascomm/features/home/presentation/bloc/home_cubit.dart';
import 'package:mini_project_vascomm/core/utils/injector.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:mini_project_vascomm/features/authentication/presentation/pages/auth_page.dart';
import 'package:mini_project_vascomm/features/home/presentation/pages/home_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  /// Initialize Dependency Injection
  await configureDependencies();

  /// Initialize Dio and Base URL
  DioHelper.initialDio(URLConstant.baseURL);

  /// Get User Session Info
  final sharedPref = getIt<SharedPreferences>();
  bool? isLoggedIn = sharedPref.getBool(StorageConstant.isLoggedInKey);

  runApp(
    MyApp(
      isLoggedIn: isLoggedIn,
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({
    super.key,
    required this.isLoggedIn,
  });
  final bool? isLoggedIn;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => getIt<AuthBloc>()),
        BlocProvider(
          create: (context) => getIt<HomeCubit>()..initializeProducts(),
        ),
      ],
      child: MaterialApp(
        title: 'Mini Project for Vascomm',
        debugShowCheckedModeBanner: false,
        theme: AppTheme.lightTheme,
        home:
            isLoggedIn == true ? const HomePage() : const AuthenticationPage(),
      ),
    );
  }
}
