import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project_vascomm/core/utils/injector.config.dart';

final getIt = GetIt.instance;

@InjectableInit(generateForDir: ['lib', 'test'])
Future<void> configureDependencies() async => getIt.init();
