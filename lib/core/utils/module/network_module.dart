import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:mini_project_vascomm/core/helpers/dio_helper.dart';

@module
abstract class NetworkModule {
  @lazySingleton
  Dio get dio => DioHelper.dio!;
}
