import 'package:flutter/material.dart';

abstract class ActionState {
  const factory ActionState.initial() = ActionInitial;

  const factory ActionState.loading() = ActionLoading;

  const factory ActionState.success() = ActionSuccess;

  factory ActionState.error(Object error, {StackTrace? stackTrace}) =
      ActionError;

  // private mapper, so that classes inheriting ActionState can specify their own
  // `map` method with different parameters.
  R map<R>({
    required R Function(ActionInitial initial) initial,
    required R Function(ActionLoading loading) loading,
    required R Function(ActionSuccess success) success,
    required R Function(ActionError error) error,
  });
}

/// Creates an [ActionState] with a data.
///
/// The data can be `null`.
class ActionSuccess implements ActionState {
  /// Creates an [ActionState] with a data.
  ///
  /// The data can be `null`.
  const ActionSuccess();

  @override
  R map<R>({
    required R Function(ActionInitial initial) initial,
    required R Function(ActionLoading loading) loading,
    required R Function(ActionSuccess success) success,
    required R Function(ActionError error) error,
  }) {
    return success(this);
  }

  @override
  String toString() {
    return 'ActionSuccess()';
  }

  @override
  bool operator ==(Object other) {
    return runtimeType == other.runtimeType;
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

/// Creates an [ActionState] in Initial state.
///
/// Prefer always using this constructor with the `const` keyword.
class ActionInitial implements ActionState {
  /// Creates an [ActionState] in loading state.
  ///
  /// Prefer always using this constructor with the `const` keyword.
  const ActionInitial();

  @override
  R map<R>({
    required R Function(ActionInitial initial) initial,
    required R Function(ActionLoading loading) loading,
    required R Function(ActionError error) error,
    required R Function(ActionSuccess success) success,
  }) {
    return initial(this);
  }

  @override
  String toString() {
    return 'ActionInitial()';
  }

  @override
  bool operator ==(Object other) {
    return runtimeType == other.runtimeType;
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

/// Creates an [ActionState] in loading state.
///
/// Prefer always using this constructor with the `const` keyword.
class ActionLoading implements ActionState {
  /// Creates an [ActionState] in loading state.
  ///
  /// Prefer always using this constructor with the `const` keyword.
  const ActionLoading();

  @override
  R map<R>({
    required R Function(ActionInitial initial) initial,
    required R Function(ActionLoading loading) loading,
    required R Function(ActionSuccess success) success,
    required R Function(ActionError error) error,
  }) {
    return loading(this);
  }

  @override
  String toString() {
    return 'ActionLoading()';
  }

  @override
  bool operator ==(Object other) {
    return runtimeType == other.runtimeType;
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

/// Creates an [ActionState] in error state.
///
/// The parameter [error] cannot be `null`.
@immutable
class ActionError implements ActionState {
  /// Creates an [ActionState] in error state.
  const ActionError(this.error, {this.stackTrace});

  /// The error.
  final Object error;

  /// The stacktrace of [error].
  final StackTrace? stackTrace;

  @override
  R map<R>({
    required R Function(ActionInitial initial) initial,
    required R Function(ActionLoading loading) loading,
    required R Function(ActionSuccess success) success,
    required R Function(ActionError error) error,
  }) {
    return error(this);
  }

  @override
  String toString() {
    return 'ActionError(error: $error, stackTrace: $stackTrace)';
  }

  @override
  bool operator ==(Object other) {
    return runtimeType == other.runtimeType &&
        other is ActionError &&
        other.error == error &&
        other.stackTrace == stackTrace;
  }

  @override
  int get hashCode => Object.hash(runtimeType, error, stackTrace);
}
