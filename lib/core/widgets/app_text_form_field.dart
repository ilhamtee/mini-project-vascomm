import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';
import 'package:mini_project_vascomm/core/styles/typography.dart';

class AppTextFormField extends StatefulWidget {
  const AppTextFormField({
    super.key,
    required this.controller,
    required this.hintText,
    this.label,
    this.isPassword = false,
    this.isForgotPassword = false,
    this.style,
    this.hintStyle,
    this.inputBorder,
    this.contentPadding,
    this.suffixIcon,
    this.keyboardType,
    this.inputFormatters,
    this.borderRadius,
    this.elevation,
    this.focusNode,
  });

  final TextEditingController controller;
  final FocusNode? focusNode;
  final String? label;
  final TextStyle? style;
  final TextStyle? hintStyle;
  final String hintText;
  final InputBorder? inputBorder;
  final EdgeInsets? contentPadding;
  final bool isPassword;
  final bool isForgotPassword;
  final Widget? suffixIcon;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final BorderRadiusGeometry? borderRadius;
  final double? elevation;

  @override
  State<AppTextFormField> createState() => _AppTextFormFieldState();
}

class _AppTextFormFieldState extends State<AppTextFormField> {
  late bool _obscureText = widget.isPassword;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: [
            if (widget.label != null && widget.label?.isNotEmpty == true)
              Text(
                widget.label!,
                style: Typographies.boldDefaultStyle.copyWith(fontSize: 13),
              ),
            if (widget.isForgotPassword)
              InkWell(
                onTap: () {},
                child: Text(
                  'Lupa Password anda ?',
                  style: Typographies.boldDefaultStyle.copyWith(
                    fontSize: 12,
                    color: AppColors.primary100,
                  ),
                ),
              ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Material(
          elevation: widget.elevation ?? 1.0,
          color: Colors.white,
          borderRadius: widget.borderRadius ?? BorderRadius.circular(8.0),
          child: TextFormField(
            controller: widget.controller,
            focusNode: widget.focusNode,
            style: widget.style ??
                const TextStyle(
                  color: AppColors.primary,
                  fontWeight: FontWeight.bold,
                ),
            obscureText: _obscureText,
            inputFormatters: widget.inputFormatters,
            keyboardType: widget.keyboardType,
            decoration: InputDecoration(
              hintText: widget.hintText,
              hintStyle: widget.hintStyle ??
                  const TextStyle(
                    color: AppColors.grey500,
                    fontWeight: FontWeight.w500,
                    fontSize: 13,
                  ),
              border: widget.inputBorder ?? InputBorder.none,
              suffixIcon: widget.isPassword
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          _obscureText = !_obscureText;
                        });
                      },
                      padding: const EdgeInsets.all(12),
                      icon: Image.asset(
                        _obscureText
                            ? 'assets/icons/view-on.png'
                            : 'assets/icons/view-off.png',
                        width: 30,
                      ),
                    )
                  : widget.suffixIcon,
              contentPadding: widget.contentPadding ??
                  const EdgeInsets.symmetric(
                    vertical: 18.0,
                    horizontal: 12.0,
                  ),
            ),
          ),
        ),
      ],
    );
  }
}
