import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ToastWidget {
  ToastWidget._();

  static void show({
    required String message,
    required Color backgroundColor,
    required Color textColor,
    double fontSize = 14.0,
  }) {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 2,
      textColor: textColor,
      backgroundColor: backgroundColor,
      fontSize: fontSize,
    );
  }
}
