import 'package:intl/intl.dart';

class NumberFormatterHelper {
  NumberFormatterHelper._();

  static String formatRupiah(double value) {
    final numberFormat = NumberFormat.currency(
      locale: 'id_ID',
      symbol: 'Rp ',
      decimalDigits: 0,
    );
    return numberFormat.format(value);
  }
}
