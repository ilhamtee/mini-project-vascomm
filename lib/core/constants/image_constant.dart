class ImageConstant {
  ImageConstant._();

  static const String avatar = 'assets/images/avatar.png';
  static const String calendarTimeline = 'assets/images/calendar_timeline.png';
  static const String examinationPlace = 'assets/images/examination_place.png';
  static const String headerLogin = 'assets/images/header_login.png';
  static const String product = 'assets/images/product.png';
  static const String track = 'assets/images/track.png';
  static const String vaccine = 'assets/images/vaccine.png';
}
