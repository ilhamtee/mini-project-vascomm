class URLConstant {
  URLConstant._();

  static const String baseURL = 'https://reqres.in/api';
  static const String login = '/login';
}
