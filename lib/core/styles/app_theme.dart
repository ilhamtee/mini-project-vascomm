import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';

class AppTheme {
  AppTheme._();

  /// Default Light Theme Configuration
  static ThemeData lightTheme = ThemeData(
    fontFamily: 'Gilroy',
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        backgroundColor: AppColors.primary,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        fixedSize: const Size(double.infinity, 40.0),
        padding: const EdgeInsets.all(8.0),
      ),
    ),
  );
}
