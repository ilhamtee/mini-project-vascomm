import 'package:flutter/material.dart';
import 'package:mini_project_vascomm/core/styles/app_colors.dart';

class Typographies {
  Typographies._();

  static const String _baseFont = 'Gilroy';

  static const TextStyle regularDefaultStyle = TextStyle(
    fontFamily: _baseFont,
    color: AppColors.primary,
    fontSize: 12,
  );

  static const TextStyle boldDefaultStyle = TextStyle(
    fontFamily: _baseFont,
    color: AppColors.primary,
    fontSize: 14,
    fontWeight: FontWeight.bold,
  );
}
