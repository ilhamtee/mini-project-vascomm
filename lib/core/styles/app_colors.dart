import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  // Primary Colors
  static const Color primary = Color(0xFF002060);
  static const Color primary200 = Color(0xFF1A3E78);
  static const Color primary100 = Color(0xFF597393);
  static const Color primary50 = Color(0xFFDAE9F9);
  static const Color primary10 = Color(0xFFF7F9FF);

  // Grey Colors
  static const Color grey50 = Color(0xFFEBEDF7);
  static const Color grey100 = Color(0xFF8C8C8C);
  static const Color grey400 = Color(0xFF383838);
  static const Color grey500 = Color(0xFF2A2A2A);

  // Orange Colors
  static const Color orange01 = Color(0xFFFF7200);

  // Green Colors
  static const Color green01 = Color(0xFFB3FFCB);
  static const Color green02 = Color(0xFF007025);

  // Blue Colors
  static const Color blue01 = Color(0xFF00D9D5);
}
